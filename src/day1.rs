use std::fs::File;
use std::io::{BufRead, BufReader};

fn read_depths(path: &str) -> Vec<i32> {
    let file = File::open(path).expect("Unable to open file");

    BufReader::new(file).lines()
        .map(|l| { l.expect("Unable to read line") })
        .map(|l| { l.parse().expect("Unable to parse line to int") })
        .collect()
}

fn count(depths: Vec<i32>) -> i32 {
    let mut prev: Option<i32> = None;
    let mut count = 0;

    for depth in depths {
        if let Some(prev) = prev {
            if depth > prev {
                count = count + 1;
            }
        }

        prev = Some(depth);
    }

    count
}

fn window(path: &str) -> Vec<i32> {
    let depths = read_depths(path);

    let mut out: Vec<i32> = vec![];
    for i in 0..(depths.len()-2) {
        out.push(depths[i] + depths[i+1] + depths[i+2]);
    }

    out
}

pub fn run() {
    println!("day 1, part 1: {}", count(read_depths("input/day1")));
    println!("day 1, part 2: {}", count(window("input/day1")));
}
