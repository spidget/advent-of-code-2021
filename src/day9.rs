use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn run() {
    let input = parse("input/day9").expect("Unable to parse input");

    println!("Day 9, part 1: {}", risk_level(input.clone()));
    println!("Day 9, part 2: {}", basin_sizes(input));
}

fn parse_line(line: String) -> Result<Vec<u32>, String> {
    line.chars()
        .map(|c| c.to_digit(10).ok_or("Failed to parse digit".to_string()))
        .collect()
}

fn parse(path: &str) -> Result<Vec<Vec<u32>>, String> {
    let file = File::open(path).map_err(|e| format!("Unable to open file: {}", e))?;
    BufReader::new(file)
        .lines()
        .map(|l| {
            let line = l.map_err(|e| format!("Unable to read line: {}", e))?;
            parse_line(line)
        })
        .collect()
}

fn basin_sizes(mut height_map: Vec<Vec<u32>>) -> usize {
    let wall = 9;
    let used = u32::MAX;

    let mut basins: Vec<HashSet<(usize, usize)>> = Vec::new();

    for y in 0..height_map.len() {
        for x in 0..height_map[y].len() {
            let mut basin: HashSet<(usize, usize)> = HashSet::new();
            let mut queue: Vec<(usize, usize)> = vec![(x, y)];

            while let Some((x, y)) = queue.pop() {
                if let Some(height) = height_map.get(y).and_then(|r| r.get(x)) {
                    if *height != wall && *height != used {
                        basin.insert((x, y));
                        height_map[y][x] = used;

                        if y != 0 {
                            queue.push((x, y - 1));
                        }
                        queue.push((x, y + 1));
                        if x != 0 {
                            queue.push((x - 1, y));
                        }
                        queue.push((x + 1, y));
                    }
                }
            }

            if !basin.is_empty() {
                basins.push(basin);
            }
        }
    }

    let mut basins = basins.iter().map(|b| b.len()).collect::<Vec<usize>>();
    basins.sort_by_key(|b| std::cmp::Reverse(*b));

    let mut iter = basins.iter();
    if let (Some(a), Some(b), Some(c)) = (iter.next(), iter.next(), iter.next()) {
        a * b * c
    } else {
        0
    }
}

fn risk_level(height_map: Vec<Vec<u32>>) -> usize {
    let mut risk_level = 0;
    for y in 0..height_map.len() {
        for x in 0..height_map[y].len() {
            let left = match x {
                0 => None,
                x => height_map[y].get(x - 1),
            };
            let above = match y {
                0 => None,
                y => height_map.get(y - 1).and_then(|r| r.get(x)),
            };
            let right = height_map[y].get(x + 1);
            let below = height_map.get(y + 1).and_then(|r| r.get(x));

            let val = height_map[y][x];

            let low_point = [left, above, right, below]
                .iter()
                .filter(|x| x.is_some())
                .map(|x| x.expect("checked above"))
                .all(|x| val < *x);

            if low_point {
                risk_level += val as usize + 1;
            }
        }
    }
    risk_level
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example() {
        let input = vec![
            vec![2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
            vec![3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
            vec![9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
            vec![8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
            vec![9, 8, 9, 9, 9, 6, 5, 6, 7, 8],
        ];

        assert_eq!(risk_level(input), 15);
    }

    #[test]
    fn example_2() {
        let input = vec![
            vec![2, 1, 9, 9, 9, 4, 3, 2, 1, 0],
            vec![3, 9, 8, 7, 8, 9, 4, 9, 2, 1],
            vec![9, 8, 5, 6, 7, 8, 9, 8, 9, 2],
            vec![8, 7, 6, 7, 8, 9, 6, 7, 8, 9],
            vec![9, 8, 9, 9, 9, 6, 5, 6, 7, 8],
        ];

        assert_eq!(basin_sizes(input), 1134);
    }
}
