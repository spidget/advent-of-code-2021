use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn run() {
    let result = parse("input/day10").expect("Failed to parse file");

    let part1: usize = result
        .iter()
        .map(|r| match r {
            LineStatus::Corrupted(x) => *x,
            _ => 0,
        })
        .sum();

    println!("Day 10, part 1: {}", part1);

    let mut part2: Vec<usize> = result
        .into_iter()
        .filter_map(|r| match r {
            LineStatus::Incomplete(x) => Some(x),
            _ => None,
        })
        .collect();
    part2.sort();

    println!("Day 10, part 1: {}", part2[part2.len() / 2]);
}

fn parse(path: &str) -> Result<Vec<LineStatus>, Box<dyn Error>> {
    let file = File::open(path)?;

    BufReader::new(file)
        .lines()
        .map(|l| -> Result<LineStatus, Box<dyn Error>> { Ok(parse_line(&l?)) })
        .collect::<Result<Vec<LineStatus>, Box<dyn Error>>>()
}

#[derive(Debug, PartialEq)]
enum LineStatus {
    Ok,
    Corrupted(usize),
    Incomplete(usize),
}

fn parse_line(line: &str) -> LineStatus {
    let mut stack = Vec::new();

    for c in line.chars() {
        match c {
            '{' | '[' | '(' | '<' => stack.push(c),
            '}' => {
                if stack.pop() != Some('{') {
                    return LineStatus::Corrupted(1197);
                }
            }
            ']' => {
                if stack.pop() != Some('[') {
                    return LineStatus::Corrupted(57);
                }
            }
            ')' => {
                if stack.pop() != Some('(') {
                    return LineStatus::Corrupted(3);
                }
            }
            '>' => {
                if stack.pop() != Some('<') {
                    return LineStatus::Corrupted(25137);
                }
            }
            _ => println!("Unknown char: {}", c),
        }
    }


    if !stack.is_empty() {
        let mut score = 0;
        while let Some(c) = stack.pop() {
            score *= 5;
            match c {
                '(' => score += 1,
                '[' => score += 2,
                '{' => score += 3,
                '<' => score += 4,
                _ => println!("Unknown char: {}", c),
            }
        }
        LineStatus::Incomplete(score)
    } else {
        LineStatus::Ok
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_1() {
        assert_eq!(parse_line("{([(<{}[<>[]}>{[]{[(<()>"), LineStatus::Corrupted(1197));
    }

    #[test]
    fn example_2() {
        assert_eq!(parse_line("[[<[([]))<([[{}[[()]]]"), LineStatus::Corrupted(3));
    }

    #[test]
    fn example_3() {
        assert_eq!(parse_line("[{[{({}]{}}([{[{{{}}([]"), LineStatus::Corrupted(57));
    }

    #[test]
    fn example_4() {
        assert_eq!(parse_line("[<(<(<(<{}))><([]([]()"), LineStatus::Corrupted(3));
    }

    #[test]
    fn example_5() {
        assert_eq!(parse_line("<{([([[(<>()){}]>(<<{{"), LineStatus::Corrupted(25137));
    }

    #[test]
    fn valid_example_1() {
        assert_eq!(parse_line("()"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_2() {
        assert_eq!(parse_line("[]"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_3() {
        assert_eq!(parse_line("([])"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_4() {
        assert_eq!(parse_line("{()()()}"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_5() {
        assert_eq!(parse_line("<([{}])>"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_6() {
        assert_eq!(parse_line("[<>({}){}[([])<>]]"), LineStatus::Ok);
    }

    #[test]
    fn valid_example_7() {
        assert_eq!(parse_line("(((((((((())))))))))"), LineStatus::Ok);
    }

    #[test]
    fn incomplete_example_1() {
        assert_eq!(parse_line("[({(<(())[]>[[{[]{<()<>>"), LineStatus::Incomplete(288957));
    }

    #[test]
    fn incomplete_example_2() {
        assert_eq!(parse_line("[(()[<>])]({[<{<<[]>>("), LineStatus::Incomplete(5566));
    }

    #[test]
    fn incomplete_example_3() {
        assert_eq!(parse_line("(((({<>}<{<{<>}{[]{[]{}"), LineStatus::Incomplete(1480781));
    }

    #[test]
    fn incomplete_example_4() {
        assert_eq!(parse_line("{<[[]]>}<{[{[{[]{()[[[]"), LineStatus::Incomplete(995444));
    }

    #[test]
    fn incomplete_example_5() {
        assert_eq!(parse_line("<{([{{}}[<[[[<>{}]]]>[]]"), LineStatus::Incomplete(294));
    }
}
