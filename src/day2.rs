use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
enum Direction {
    Forward(i32),
    Down(i32),
    Up(i32),
}

#[derive(Debug)]
enum DirectionErrors {
    CantOpenFile(String),
    NoDirection,
    NoDistance,
    InvalidDistance(String),
    InvalidDirection(String),
}

impl Error for DirectionErrors {}

impl fmt::Display for DirectionErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match self {
            DirectionErrors::CantOpenFile(s) => format!("Unable to open file: {}", s),
            DirectionErrors::NoDirection => "Missing direction".to_string(),
            DirectionErrors::NoDistance => "Missing distance".to_string(),
            DirectionErrors::InvalidDistance(s) => format!("Invalid distance: {}", s),
            DirectionErrors::InvalidDirection(s) => format!("Invalid direction: {}", s),
        })
    }
}

impl Direction {
    fn parse(s: String) -> Result<Direction, DirectionErrors> {
        let mut iter = s.split_whitespace();

        let direction = iter.next().ok_or(DirectionErrors::NoDirection)?;
        let distance = iter.next().ok_or(DirectionErrors::NoDistance)?;

        let distance = distance.parse::<i32>().map_err(|e| {
            DirectionErrors::InvalidDistance(e.to_string())
        })?;

        match direction {
            "forward" => Ok(Direction::Forward(distance)),
            "down" => Ok(Direction::Down(distance)),
            "up" => Ok(Direction::Up(distance)),
            _ => Err(DirectionErrors::InvalidDirection(direction.to_string())),
        }
    }
}

fn read_commands(path: &str) -> Result<Vec<Direction>, DirectionErrors> {
    let file = File::open(path).map_err(|e| {
        DirectionErrors::CantOpenFile(e.to_string())
    })?;

    BufReader::new(file).lines()
        .map(|l| { l.expect("Unable to read line") })
        .map(|l| { Direction::parse(l) })
        .collect()
}

fn part1(commands: &Vec<Direction>) {
    let mut horizontal = 0;
    let mut depth = 0;

    for command in commands {
        match command {
            Direction::Forward(distance) => horizontal += distance,
            Direction::Down(distance) => depth += distance,
            Direction::Up(distance) => depth -= distance,
        }
    }

    println!("day 2, part 1: {}", horizontal * depth);
}

fn part2(commands: &Vec<Direction>) {
    let mut horizontal = 0;
    let mut depth = 0;
    let mut aim = 0;

    for command in commands {
        match command {
            Direction::Forward(distance) => {
                horizontal += distance;
                depth += aim * distance;
            },
            Direction::Down(distance) => aim += distance,
            Direction::Up(distance) => aim -= distance,
        }
    }

    println!("day 2, part 2: {}", horizontal * depth);
}

pub fn run() {
    let commands = read_commands("input/day2").expect("Unable to read commands");

    part1(&commands);
    part2(&commands);
}
