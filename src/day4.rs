use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Lines;

#[derive(Debug)]
enum ParseErrors {
    CantOpenFile(String),
    MissingNo,
    CantReadFile(String),
    CantParseBoard(String),
}

impl Error for ParseErrors {}

impl fmt::Display for ParseErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ParseErrors::CantOpenFile(s) => format!("Unable to open file: {}", s),
                ParseErrors::MissingNo => "Missing move list in input".to_string(),
                ParseErrors::CantReadFile(s) => format!("Unable to read file: {}", s),
                ParseErrors::CantParseBoard(s) => format!("Unable to parse board: {}", s),
            }
        )
    }
}

#[derive(Debug)]
struct Game {
    moves: Vec<u8>,
    boards: Vec<[[u8; 5]; 5]>,
}

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Game\nmoves: {:?}", self.moves)?;
        for board in &self.boards {
            write!(f, "\nboard\n")?;
            for line in board {
                write!(f, "{:?}\n", line)?;
            }
        }
        Ok(())
    }
}

impl Game {
    fn is_won(result: &[[bool; 5]; 5]) -> bool {
        for line in result {
            if line[0] && line[1] && line[2] && line[3] && line[4] {
                return true;
            }
        }

        for i in 0..5 {
            if result[0][i] && result[1][i] && result[2][i] && result[3][i] && result[4][i] {
                return true;
            }
        }

        false
    }

    fn score(result: &[[bool; 5]; 5], board: &[[u8; 5]; 5], num: u8) -> u32 {
        let mut score: u32 = 0;
        for x in 0..5 {
            for y in 0..5 {
                if !result[x][y] {
                    score += board[x][y] as u32;
                }
            }
        }

        score * num as u32
    }

    fn play(&self) -> Vec<u32> {
        let mut results: Vec<[[bool; 5]; 5]> = self
            .boards
            .iter()
            .map(|_| {
                [
                    [false, false, false, false, false],
                    [false, false, false, false, false],
                    [false, false, false, false, false],
                    [false, false, false, false, false],
                    [false, false, false, false, false],
                ]
            })
            .collect();

        let mut scores = HashMap::new();
        let mut winning_order = Vec::new();

        for m in &self.moves {
            for (board_no, board) in self.boards.iter().enumerate() {
                if Game::is_won(&results[board_no]) {
                    continue;
                }

                for x in 0..5 {
                    for y in 0..5 {
                        if board[x][y] == *m {
                            results[board_no][x][y] = true;
                        }
                    }
                }

                if Game::is_won(&results[board_no]) {
                    let score = Game::score(&results[board_no], &board, *m);
                    winning_order.push(board_no);
                    scores.insert(board_no, score);
                }
            }
        }

        winning_order.iter().map(|b| scores.get(b).expect("huh?")).cloned().collect()
    }

    fn new(path: &str) -> Result<Game, ParseErrors> {
        let file = File::open(path).map_err(|e| ParseErrors::CantOpenFile(e.to_string()))?;
        let mut lines = BufReader::new(file).lines();

        let moves = lines
            .next()
            .ok_or(ParseErrors::MissingNo)?
            .map_err(|e| ParseErrors::CantReadFile(e.to_string()))?
            .split(',')
            .map(|s| {
                s.parse::<u8>()
                    .map_err(|e| ParseErrors::CantReadFile(e.to_string()))
            })
            .collect::<Result<Vec<u8>, ParseErrors>>()?;

        let mut boards = vec![];
        while let Some(Ok(_header)) = lines.next() {
            boards.push([
                Game::parse_numbers(&mut lines)?,
                Game::parse_numbers(&mut lines)?,
                Game::parse_numbers(&mut lines)?,
                Game::parse_numbers(&mut lines)?,
                Game::parse_numbers(&mut lines)?,
            ]);
        }

        Ok(Game { moves, boards })
    }

    fn parse_numbers(lines: &mut Lines<BufReader<File>>) -> Result<[u8; 5], ParseErrors> {
        let numbers = lines
            .next()
            .ok_or(ParseErrors::MissingNo)?
            .map_err(|e| ParseErrors::CantParseBoard(e.to_string()))?
            .split_whitespace()
            .map(|n| {
                n.parse::<u8>()
                    .map_err(|e| ParseErrors::CantReadFile(e.to_string()))
            })
            .collect::<Result<Vec<u8>, ParseErrors>>()?;

        let mut numbers = numbers.iter();

        Ok([
            *numbers.next().ok_or(ParseErrors::MissingNo)?,
            *numbers.next().ok_or(ParseErrors::MissingNo)?,
            *numbers.next().ok_or(ParseErrors::MissingNo)?,
            *numbers.next().ok_or(ParseErrors::MissingNo)?,
            *numbers.next().ok_or(ParseErrors::MissingNo)?,
        ])
    }
}

pub fn run() {
    let scores = Game::new("input/day4")
        .expect("failed to parse game")
        .play();

    println!("day 4, part 1: {}", scores[0]);
    println!("day 4, part 2: {}", scores.last().expect("bam"));
}
