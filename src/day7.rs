use std::cmp::{max, min};
use std::error::Error;
use std::fs::read_to_string;
use std::num::ParseIntError;

pub fn run() {
    let crabs = parse("input/day7").expect("Unable to parse crabs");

    println!("Day 7, part 1: {}", find_pos(&crabs, true).1);
    println!("Day 7, part 2: {}", find_pos(&crabs, false).1);
}

fn parse(path: &str) -> Result<Vec<u32>, Box<dyn Error>> {
    Ok(read_to_string(path)?
        .trim()
        .split(",")
        .map(|n| n.parse::<u32>())
        .collect::<Result<Vec<u32>, ParseIntError>>()?)
}

fn find_pos(crabs: &Vec<u32>, constant_fuel: bool) -> (u32, u32) {
    let end = match crabs.iter().max().cloned().unwrap_or(0) {
        0 => return (0, 0),
        i => i,
    };

    let mut min_fuel = std::u32::MAX;
    let mut result = 0;

    for i in 0..end {
        let fuel: u32 = crabs.iter().map(|c| {
            let fuel = max(*c, i) - min(*c, i);

            if constant_fuel { fuel } else { fuel_cost(fuel) }
        }).sum();
        if fuel < min_fuel {
            min_fuel = fuel;
            result = i;
        }
    }

    (result, min_fuel)
}

fn fuel_cost(steps: u32) -> u32 {
    steps * (steps + 1) / 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fuel_cost_0() {
        assert_eq!(fuel_cost(0), 0);
    }

    #[test]
    fn fuel_cost_1() {
        assert_eq!(fuel_cost(1), 1);
    }

    #[test]
    fn fuel_cost_2() {
        assert_eq!(fuel_cost(2), 3);
    }

    #[test]
    fn fuel_cost_6() {
        assert_eq!(fuel_cost(6), 21);
    }

    #[test]
    fn empty() {
        assert_eq!(find_pos(&Vec::new(), true), (0, 0));
    }

    #[test]
    fn example() {
        assert_eq!(find_pos(&vec![16, 1, 2, 0, 4, 2, 7, 1, 2, 14], true), (2, 37));
    }
}
