use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

pub fn run() {
    let mut input = parse("input/day11").expect("Unable to parse file");

    let mut count = 0;
    for _ in 0..100 {
        count += step(&mut input);
    }
    println!("Day 11, part 1: {}", count);

    let mut input = parse("input/day11").expect("Unable to parse file");

    let mut count = 1;
    while step(&mut input) != 100 {
        count += 1;
    }
    println!("Day 11, part 2: {}", count);
}

fn to_digit(c: Option<char>) -> Result<u32, Box<dyn Error>> {
    Ok(c.ok_or("Missing char")?.to_digit(10).ok_or("Unable to parse digit")?)
}

fn parse(path: &str) -> Result<[[u32; 10]; 10], Box<dyn Error>> {
    let file = File::open(path)?;
    let arrays = BufReader::new(file).lines().map(|l| {
        let line = l?;
        let mut c = line.chars();
        Ok([
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
            to_digit(c.next())?,
        ])
    }).collect::<Result<Vec<[u32; 10]>, Box<dyn Error>>>()?;

    let mut lines = arrays.into_iter();
    Ok([
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
       lines.next().ok_or("Missing line")?,
    ])
}

fn step(octopodes: &mut [[u32; 10]; 10]) -> usize {
    for y in 0..octopodes.len() {
        for x in 0..octopodes[y].len() {
            octopodes[y][x] += 1;
        }
    }

    let mut has_flashed = octopodes.iter().flat_map(|r| r.iter()).any(|o| *o == 10);
    let mut flashed = HashSet::new();

    while has_flashed {
        has_flashed = false;

        for y in 0..octopodes.len() {
            for x in 0..octopodes[y].len() {
                if octopodes[y][x] > 9 && !flashed.contains(&(x, y)) {
                    has_flashed = true;
                    flashed.insert((x, y));

                    if x != 0 && y != 0 {
                        octopodes[y - 1][x - 1] += 1;
                    }
                    if y != 0 {
                        octopodes[y - 1][x] += 1;
                    }
                    if y != 0 && x < 9 {
                        octopodes[y - 1][x + 1] += 1;
                    }

                    if x != 0 {
                        octopodes[y][x - 1] += 1;
                    }
                    if x < 9 {
                        octopodes[y][x + 1] += 1;
                    }

                    if y < 9 && x != 0 {
                        octopodes[y + 1][x - 1] += 1;
                    }
                    if y < 9 {
                        octopodes[y + 1][x] += 1;
                    }
                    if y < 9 && x < 9 {
                        octopodes[y + 1][x + 1] += 1;
                    }
                }
            }
        }
    }

    for y in 0..octopodes.len() {
        for x in 0..octopodes[y].len() {
            if octopodes[y][x] > 9 {
                octopodes[y][x] = 0;
            }
        }
    }

    flashed.len()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example_1() {
        let mut input = [
            [5, 4, 8, 3, 1, 4, 3, 2, 2, 3],
            [2, 7, 4, 5, 8, 5, 4, 7, 1, 1],
            [5, 2, 6, 4, 5, 5, 6, 1, 7, 3],
            [6, 1, 4, 1, 3, 3, 6, 1, 4, 6],
            [6, 3, 5, 7, 3, 8, 5, 4, 7, 8],
            [4, 1, 6, 7, 5, 2, 4, 6, 4, 5],
            [2, 1, 7, 6, 8, 4, 1, 7, 2, 1],
            [6, 8, 8, 2, 8, 8, 1, 1, 3, 4],
            [4, 8, 4, 6, 8, 4, 8, 5, 5, 4],
            [5, 2, 8, 3, 7, 5, 1, 5, 2, 6],
        ];

        step(&mut input);

        let result = [
            [6, 5, 9, 4, 2, 5, 4, 3, 3, 4],
            [3, 8, 5, 6, 9, 6, 5, 8, 2, 2],
            [6, 3, 7, 5, 6, 6, 7, 2, 8, 4],
            [7, 2, 5, 2, 4, 4, 7, 2, 5, 7],
            [7, 4, 6, 8, 4, 9, 6, 5, 8, 9],
            [5, 2, 7, 8, 6, 3, 5, 7, 5, 6],
            [3, 2, 8, 7, 9, 5, 2, 8, 3, 2],
            [7, 9, 9, 3, 9, 9, 2, 2, 4, 5],
            [5, 9, 5, 7, 9, 5, 9, 6, 6, 5],
            [6, 3, 9, 4, 8, 6, 2, 6, 3, 7],
        ];

        assert_eq!(result, input);
    }

    #[test]
    fn example_2() {
        let mut input = [
            [6, 5, 9, 4, 2, 5, 4, 3, 3, 4],
            [3, 8, 5, 6, 9, 6, 5, 8, 2, 2],
            [6, 3, 7, 5, 6, 6, 7, 2, 8, 4],
            [7, 2, 5, 2, 4, 4, 7, 2, 5, 7],
            [7, 4, 6, 8, 4, 9, 6, 5, 8, 9],
            [5, 2, 7, 8, 6, 3, 5, 7, 5, 6],
            [3, 2, 8, 7, 9, 5, 2, 8, 3, 2],
            [7, 9, 9, 3, 9, 9, 2, 2, 4, 5],
            [5, 9, 5, 7, 9, 5, 9, 6, 6, 5],
            [6, 3, 9, 4, 8, 6, 2, 6, 3, 7],
        ];

        step(&mut input);

        let result = [
            [8, 8, 0, 7, 4, 7, 6, 5, 5, 5],
            [5, 0, 8, 9, 0, 8, 7, 0, 5, 4],
            [8, 5, 9, 7, 8, 8, 9, 6, 0, 8],
            [8, 4, 8, 5, 7, 6, 9, 6, 0, 0],
            [8, 7, 0, 0, 9, 0, 8, 8, 0, 0],
            [6, 6, 0, 0, 0, 8, 8, 9, 8, 9],
            [6, 8, 0, 0, 0, 0, 5, 9, 4, 3],
            [0, 0, 0, 0, 0, 0, 7, 4, 5, 6],
            [9, 0, 0, 0, 0, 0, 0, 8, 7, 6],
            [8, 7, 0, 0, 0, 0, 6, 8, 4, 8],
        ];

        assert_eq!(result, input);
    }

    #[test]
    fn example_3() {
        let mut input = [
            [5, 4, 8, 3, 1, 4, 3, 2, 2, 3],
            [2, 7, 4, 5, 8, 5, 4, 7, 1, 1],
            [5, 2, 6, 4, 5, 5, 6, 1, 7, 3],
            [6, 1, 4, 1, 3, 3, 6, 1, 4, 6],
            [6, 3, 5, 7, 3, 8, 5, 4, 7, 8],
            [4, 1, 6, 7, 5, 2, 4, 6, 4, 5],
            [2, 1, 7, 6, 8, 4, 1, 7, 2, 1],
            [6, 8, 8, 2, 8, 8, 1, 1, 3, 4],
            [4, 8, 4, 6, 8, 4, 8, 5, 5, 4],
            [5, 2, 8, 3, 7, 5, 1, 5, 2, 6],
        ];

        let mut count = 0;
        for _ in 0..10 {
            count += step(&mut input);
        }

        assert_eq!(count, 204);
    }
}
