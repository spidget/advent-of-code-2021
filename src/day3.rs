use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug)]
enum ParseErrors {
    CantOpenFile(String),
    CantReadFile(String),
    CantParseBit,
}

impl Error for ParseErrors {}

impl fmt::Display for ParseErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ParseErrors::CantOpenFile(s) => format!("Unable to open file: {}", s),
                ParseErrors::CantReadFile(s) => format!("Unable to read file: {}", s),
                ParseErrors::CantParseBit => "Unable to parse bit".to_string(),
            }
        )
    }
}

struct Diagnostic([bool; 12]);

impl Diagnostic {
    fn parse_bit(c: Option<char>) -> Result<bool, ParseErrors> {
        match c {
            Some('0') => Ok(false),
            Some('1') => Ok(true),
            _ => Err(ParseErrors::CantParseBit),
        }
    }

    fn new(s: String) -> Result<[bool; 12], ParseErrors> {
        let mut c = s.chars();
        Ok([
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
            Diagnostic::parse_bit(c.next())?,
        ])
    }
}

fn parse(path: &str) -> Result<Vec<[bool; 12]>, ParseErrors> {
    let file = File::open(path).map_err(|e| ParseErrors::CantOpenFile(e.to_string()))?;

    BufReader::new(file)
        .lines()
        .map(|l| Diagnostic::new(l.map_err(|e| ParseErrors::CantReadFile(e.to_string()))?))
        .collect()
}

#[derive(Debug)]
struct BitCount([i32; 12]);

impl BitCount {
    fn new() -> BitCount {
        BitCount([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    }

    fn count(self: &mut BitCount, i: usize, b: bool) {
        if b {
            self.0[i] += 1;
        } else {
            self.0[i] -= 1;
        }
    }

    fn from_diagnostics(diagnostics: &Vec<[bool; 12]>) -> BitCount {
        let mut count = BitCount::new();

        for d in diagnostics {
            for i in 0..12 {
                count.count(i, d[i]);
            }
        }

        count
    }

    fn result<F: Fn(i32) -> bool>(self: &BitCount, check: F) -> u32 {
        let mut out = 0;

        for i in 0..12 {
            if check(self.0[i]) {
                out += 1;
            }
            out <<= 1;
        }

        out >> 1
    }

    fn gamma_rate(self: &BitCount) -> u32 {
        self.result(|i| i > 0)
    }

    fn epsilon_rate(self: &BitCount) -> u32 {
        self.result(|i| i < 0)
    }

    fn life_support_rating<F: Fn(bool, i32) -> bool>(
        diagnostics: &Vec<[bool; 12]>,
        check: F,
    ) -> Option<u32> {

        let mut i = 0;
        let mut options: Vec<[bool; 12]> = diagnostics.clone();
        loop {
            let count = BitCount::from_diagnostics(&options);
            options = options
                .iter()
                .filter(|d| check(d[i], count.0[i]))
                .cloned()
                .collect();

            if options.len() == 1 {
                let mut result = 0;

                for i in 0..12 {
                    if options[0][i] {
                        result += 1;
                    }
                    result <<= 1;
                }

                return Some(result >> 1);
            } else if options.len() == 0 {
                return None;
            }

            i += 1;
        }
    }

    fn oxygen_generator_rating(diagnostics: &Vec<[bool; 12]>) -> Option<u32> {
        BitCount::life_support_rating(diagnostics, |bit, count| bit == (count >= 0))
    }

    fn co2_scrubber_rating(diagnostics: &Vec<[bool; 12]>) -> Option<u32> {
        BitCount::life_support_rating(diagnostics, |bit, count| bit == (count < 0))
    }
}

fn part1(diagnostics: &Vec<[bool; 12]>) {
    let count = BitCount::from_diagnostics(diagnostics);

    println!(
        "day 3, part 1: {}",
        count.gamma_rate() * count.epsilon_rate()
    );
}

fn part2(diagnostics: &Vec<[bool; 12]>) {
    let ogr = BitCount::oxygen_generator_rating(diagnostics).expect("no ogr");
    let csr = BitCount::co2_scrubber_rating(diagnostics).expect("no csr");

    println!("day 3, part 2: {}", ogr * csr);
}

pub fn run() {
    let diagnostics = parse("input/day3").expect("Unable to parse data");

    part1(&diagnostics);
    part2(&diagnostics);
}
