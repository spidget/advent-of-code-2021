use std::collections::HashMap;
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug)]
enum ParseErrors {
    CantOpenFile(std::io::Error),
    CantReadFile(std::io::Error),
    CantReadPoint,
    MissingCoord,
    CantParseCoord(std::num::ParseIntError),
}

impl Error for ParseErrors {}

impl fmt::Display for ParseErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ParseErrors::CantOpenFile(e) => format!("Unable to open file: {}", e),
                ParseErrors::CantReadFile(e) => format!("Unable to read file: {}", e),
                ParseErrors::CantReadPoint => "Missing point in line".to_string(),
                ParseErrors::MissingCoord => "Missing coord in point".to_string(),
                ParseErrors::CantParseCoord(e) => format!("Unable to parse coord: {}", e),
            }
        )
    }
}

#[derive(Copy, Clone, Debug)]
struct Line {
    start: [usize; 2],
    end: [usize; 2],
}

impl IntoIterator for Line {
    type Item = [usize; 2];
    type IntoIter = LineIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        LineIntoIter {
            line: self,
            cur: None,
        }
    }
}

struct LineIntoIter {
    line: Line,
    cur: Option<[usize; 2]>,
}

impl Iterator for LineIntoIter {
    type Item = [usize; 2];

    fn next(&mut self) -> Option<Self::Item> {
        match self.cur {
            None => {
                self.cur = Some(self.line.start);
                return Some(self.line.start);
            }
            Some(cur) => {
                if cur[0] == self.line.end[0] && cur[1] == self.line.end[1] {
                    return None;
                }

                let next = [
                    if self.line.end[0] > cur[0] {
                        cur[0] + 1
                    } else if self.line.end[0] < cur[0] {
                        cur[0] - 1
                    } else {
                        cur[0]
                    },
                    if self.line.end[1] > cur[1] {
                        cur[1] + 1
                    } else if self.line.end[1] < cur[1] {
                        cur[1] - 1
                    } else {
                        cur[1]
                    },
                ];

                self.cur = Some(next);
                self.cur
            }
        }
    }
}

fn to_coord(point: Option<&str>) -> Result<usize, ParseErrors> {
    point
        .ok_or(ParseErrors::MissingCoord)?
        .parse::<usize>()
        .map_err(|e| ParseErrors::CantParseCoord(e))
}

fn to_point(point: Option<&str>) -> Result<[usize; 2], ParseErrors> {
    let mut coords = point.ok_or(ParseErrors::CantReadPoint)?.split(",");
    Ok([to_coord(coords.next())?, to_coord(coords.next())?])
}

fn parse() -> Result<Vec<Line>, ParseErrors> {
    let file = File::open("input/day5").map_err(|e| ParseErrors::CantOpenFile(e))?;
    BufReader::new(file)
        .lines()
        .map(|l| {
            let line = l.map_err(|e| ParseErrors::CantReadFile(e))?;
            let mut points = line.split(" -> ");

            Ok(Line {
                start: to_point(points.next())?,
                end: to_point(points.next())?,
            })
        })
        .collect::<Result<Vec<Line>, ParseErrors>>()
}

pub fn range(start: usize, end: usize) -> std::ops::Range<usize> {
    if start <= end {
        start..(end + 1)
    } else {
        end..(start + 1)
    }
}

pub fn count(counter: &mut HashMap<[usize; 2], u32>, point: [usize; 2]) {
    let count = match counter.get(&point) {
        Some(i) => i + 1,
        None => 1,
    };
    counter.insert(point, count);
}

fn part1(lines: &Vec<Line>) -> usize {
    let mut counter: HashMap<[usize; 2], u32> = HashMap::new();

    for line in lines {
        if line.start[0] == line.end[0] {
            for y in range(line.start[1], line.end[1]) {
                count(&mut counter, [line.start[0], y]);
            }
        } else if line.start[1] == line.end[1] {
            for x in range(line.start[0], line.end[0]) {
                count(&mut counter, [x, line.start[1]]);
            }
        }
    }

    counter.values().filter(|&v| v > &1).count()
}

fn part2(lines: &Vec<Line>) -> usize {
    let mut counter: HashMap<[usize; 2], u32> = HashMap::new();

    for line in lines {
        for point in *line {
            count(&mut counter, point);
        }
    }

    counter.values().filter(|&v| v > &1).count()
}

pub fn run() {
    let lines = parse().expect("Unable to parse file");
    println!("day 5, part 1: {}", part1(&lines));
    println!("day 5, part 2: {}", part2(&lines));
}
