use std::error::Error;
use std::fs::read_to_string;
use std::num::ParseIntError;

fn day(lantern_fish: &mut [usize; 9]) {
    let children = lantern_fish[0];

    for i in 0..8 {
        lantern_fish[i] = lantern_fish[i + 1];
    }

    lantern_fish[8] = children;
    lantern_fish[6] += children;
}

fn simulate(lantern_fish: &Vec<u8>, days: u32) -> usize {
    let mut simulation = [0; 9];
    for f in lantern_fish {
        simulation[*f as usize] += 1;
    }

    for _ in 0..days {
        day(&mut simulation);
    }

    simulation
        .into_iter()
        .reduce(|a, b| a + b)
        .expect("will never be none")
}

fn parse(path: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    Ok(read_to_string(path)?
        .trim()
        .split(',')
        .map(|f| f.parse::<u8>())
        .collect::<Result<Vec<u8>, ParseIntError>>()?)
}

pub fn run() {
    let lantern_fish = parse("input/day6").expect("Unable to parse file");

    println!("Day 6, part 1: {}", simulate(&lantern_fish, 80));
    println!("Day 6, part 2: {}", simulate(&lantern_fish, 256));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example_1() {
        assert_eq!(simulate(&vec![3, 4, 3, 1, 2], 18), 26);
    }

    #[test]
    fn test_example_2() {
        assert_eq!(simulate(&vec![3, 4, 3, 1, 2], 80), 5934);
    }
}
