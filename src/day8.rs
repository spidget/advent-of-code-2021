use std::collections::HashMap;
use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(Debug, Eq, PartialEq, Hash)]
struct Segments {
    a: bool,
    b: bool,
    c: bool,
    d: bool,
    e: bool,
    f: bool,
    g: bool,
}

impl Segments {
    fn count(&self) -> u8 {
        let mut field_count = 0;
        if self.a {
            field_count += 1;
        }
        if self.b {
            field_count += 1;
        }
        if self.c {
            field_count += 1;
        }
        if self.d {
            field_count += 1;
        }
        if self.e {
            field_count += 1;
        }
        if self.f {
            field_count += 1;
        }
        if self.g {
            field_count += 1;
        }
        field_count
    }

    fn is_unique_digit(&self) -> bool {
        let field_count = self.count();
        field_count == 2 || field_count == 4 || field_count == 3 || field_count == 7
    }

    fn contains_digits(&self, other: &Segments) -> bool {
        if (other.a && !self.a)
            || (other.b && !self.b)
            || (other.c && !self.c)
            || (other.d && !self.d)
            || (other.e && !self.e)
            || (other.f && !self.f)
            || (other.g && !self.g)
        {
            false
        } else {
            true
        }
    }

    fn contains_all(&self, others: Vec<&Segments>) -> bool {
        for other in others {
            if !self.contains_digits(other) {
                return false;
            }
        }
        true
    }

    fn new() -> Segments {
        Segments {
            a: false,
            b: false,
            c: false,
            d: false,
            e: false,
            f: false,
            g: false,
        }
    }

    fn from_str(segments: &str) -> Result<Segments, String> {
        let mut result = Segments::new();
        for c in segments.chars() {
            match c {
                'a' => result.a = true,
                'b' => result.b = true,
                'c' => result.c = true,
                'd' => result.d = true,
                'e' => result.e = true,
                'f' => result.f = true,
                'g' => result.g = true,
                _ => return Err("Failed to parse segment".to_string()),
            }
        }
        Ok(result)
    }

    fn parse(segments: Option<&str>) -> Result<Segments, String> {
        Segments::from_str(segments.ok_or("Missing segment")?)
    }
}

#[derive(Debug)]
struct Line {
    signals: [Segments; 10],
    displays: [Segments; 4],
}

impl Line {
    fn from_str(line: String) -> Result<Line, Box<dyn Error>> {
        let mut parts = line.split(" | ");
        let mut signals = parts.next().ok_or("No signals")?.split_whitespace();
        let mut displays = parts.next().ok_or("No displays")?.split_whitespace();

        Ok(Line {
            signals: [
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
                Segments::parse(signals.next())?,
            ],
            displays: [
                Segments::parse(displays.next())?,
                Segments::parse(displays.next())?,
                Segments::parse(displays.next())?,
                Segments::parse(displays.next())?,
            ],
        })
    }

    fn signal_with_digits(&self, digits: u8) -> Result<&Segments, String> {
        self.signals
            .iter()
            .filter(|s| s.count() == digits)
            .next()
            .ok_or(format!("no signal with digits: {}", digits))
    }

    fn decode_signals(&self) -> Result<HashMap<&Segments, u8>, String> {
        let one = self.signal_with_digits(2)?;
        let four = self.signal_with_digits(4)?;
        let seven = self.signal_with_digits(3)?;
        let eight = self.signal_with_digits(7)?;

        let mut fives: HashSet<&Segments> =
            self.signals.iter().filter(|s| s.count() == 5).collect();
        let mut sixes: HashSet<&Segments> =
            self.signals.iter().filter(|s| s.count() == 6).collect();

        let three = fives
            .iter()
            .filter(|s| s.contains_all(vec![one, seven]))
            .next()
            .cloned()
            .ok_or("no three")?;
        fives.remove(three);

        let nine = sixes
            .iter()
            .filter(|s| s.contains_all(vec![one, three, seven]))
            .next()
            .cloned()
            .ok_or("no nine")?;
        sixes.remove(nine);

        let zero = sixes
            .iter()
            .filter(|s| s.contains_all(vec![one, seven]))
            .next()
            .cloned()
            .ok_or("no zero")?;
        sixes.remove(zero);

        let six = sixes.iter().next().ok_or("no six")?;

        let five = fives
            .iter()
            .filter(|s| six.contains_digits(s))
            .next()
            .cloned()
            .ok_or("no five")?;
        fives.remove(five);

        let two = fives.iter().next().ok_or("no two")?;

        let mut result = HashMap::new();
        result.insert(zero, 0);
        result.insert(one, 1);
        result.insert(two, 2);
        result.insert(three, 3);
        result.insert(four, 4);
        result.insert(five, 5);
        result.insert(six, 6);
        result.insert(seven, 7);
        result.insert(eight, 8);
        result.insert(nine, 9);
        Ok(result)
    }

    fn value(&self) -> Result<usize, String> {
        let signals = self.decode_signals()?;

        let digits: usize = (*signals
            .get(&self.displays[3])
            .ok_or("Failed to parse value")?)
        .into();
        let tens: usize = (*signals
            .get(&self.displays[2])
            .ok_or("Failed to parse value")?)
        .into();
        let hundreds: usize = (*signals
            .get(&self.displays[1])
            .ok_or("Failed to parse value")?)
        .into();
        let thousands: usize = (*signals
            .get(&self.displays[0])
            .ok_or("Failed to parse value")?)
        .into();

        Ok(digits + (tens * 10) + (hundreds * 100) + (thousands * 1000))
    }
}

fn parse(path: &str) -> Result<Vec<Line>, Box<dyn Error>> {
    let file = File::open(path)?;

    BufReader::new(file)
        .lines()
        .map(|l| Line::from_str(l?))
        .collect()
}

pub fn run() {
    let input = parse("input/day8").expect("failed to parse input");

    let result: usize = input
        .iter()
        .map(|l| l.displays.iter().filter(|d| d.is_unique_digit()).count())
        .sum();

    println!("Day 8, part 1: {}", result);

    let result: usize = input
        .iter()
        .map(|l| l.value().expect("Unable to parse value"))
        .sum();

    println!("Day 8, part 2: {}", result);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty() {
        assert_eq!(Segments::new().is_unique_digit(), false);
    }

    #[test]
    fn test_one() {
        let mut display = Segments::new();
        display.a = true;
        display.b = true;

        assert_eq!(display.is_unique_digit(), true);
    }

    #[test]
    fn test_from_str_empty() {
        let display = Segments::new();

        assert_eq!(Segments::from_str(""), Ok(display));
    }

    #[test]
    fn test_from_str() {
        let mut display = Segments::new();
        display.a = true;
        display.b = true;
        display.c = true;
        display.d = true;
        display.e = true;
        display.f = true;
        display.g = true;

        assert_eq!(Segments::from_str("abcdefg"), Ok(display));
    }

    #[test]
    fn test_decode() {
        let input =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
                .to_string();
        let line = Line::from_str(input).expect("Failed to parse input");

        assert_eq!(line.value(), Ok(5353));
    }
}
